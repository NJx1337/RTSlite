// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RTSliteGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RTSLITE_API ARTSliteGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
